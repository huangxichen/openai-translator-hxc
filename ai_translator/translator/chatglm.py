
from transformers import AutoTokenizer, AutoModel

tokenizer = AutoTokenizer.from_pretrained("THUDM/chatglm3-6b", trust_remote_code=True)
model = AutoModel.from_pretrained("THUDM/chatglm3-6b", trust_remote_code=True).float().eval()
def chat(self, tokenizer, text, history):
    response, history = model.chat(tokenizer, text, history=history)
    print(response)



if __name__ == "__main__":
    chat(tokenizer,"你好", [])